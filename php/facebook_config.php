<?php
$app_id = getenv("849361511792122");
$app_secret = getenv("f6a7616c76ce086d8696619f341f7e21");
$app_namespace = getenv("wish-friends");
$app_url = 'https://apps.facebook.com/' . $app_namespace . '/';
$scope = 'email,publish_actions';

// Init the Facebook SDK
$facebook = new Facebook(array(
    'appId'  => $app_id,
    'secret' => $app_secret,
));
// Get the current user
$user_id = $facebook->getUser();